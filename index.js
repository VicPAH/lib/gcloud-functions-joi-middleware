const { HttpError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');

exports.validateBody = (schema, status = 400, opts = {}) => function validateBody(req, res, next) {
  const { error, value } = schema
    .label('body')
    .validate(req.body, opts);

  if (error) {
    throw new HttpError(error.details, status);
  }

  req.unvalidatedBody = req.body;
  req.body = value;

  return next();
};
