const Joi = require('joi');

const { HttpError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');

const { validateBody } = require('.');

describe('validateBody()', () => {
  test('validates an invalid body', () => {
    const schema = Joi.object({});
    expect(() => {
      try {
        validateBody(schema)({ body: null });
      } catch (err) {
        expect(err.toJSON()).toEqual({
          errors: [
            {
              message: '"body" must be of type object',
              path: [],
              type: 'object.base',
            },
          ],
        });
        throw err;
      }
    })
      .toThrowError(HttpError);
  });
  test('validates required body', () => {
    const schema = Joi.object({}).required();
    expect(() => {
      try {
        validateBody(schema)({});
      } catch (err) {
        expect(err.toJSON()).toEqual({
          errors: [
            {
              message: '"body" is required',
              path: [],
              type: 'any.required',
            },
          ],
        });
        throw err;
      }
    })
      .toThrowError(HttpError);
  });
  for (const [arg, exp] of [[undefined, 400], [500, 500], [401, 401]]) {
    test(`status arg of ${arg} results in status ${exp}`, () => {
      const schema = Joi.object({ test: Joi.string() });
      expect(() => {
        try {
          validateBody(schema, arg)({ body: null });
        } catch (err) {
          expect(err.status).toBe(exp);
          throw err;
        }
      })
        .toThrowError(HttpError);
    });
  }
});
